package selectionList;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextField;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.BorderPane;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.sun.javafx.tk.Toolkit;


/**
 * 
 * @author Aur�lien
 * 
 * Controller du FXML , gestion liste , double click et drag and drop
 *
 *
 * @param <T> g�n�rique
 */
public class ListSelection<T> extends BorderPane {

	@FXML
	ListView<T> tableDisponible;
	@FXML
	ListView<T> tableSelectionne;
	@FXML
	TableColumn<T, String> colonneDisponible;
	@FXML
	TableColumn<T, String> colonneSelectionne;
	@FXML
	Button selectionSimple;
	@FXML
	Button unselectionSimple;
	@FXML
	Button selectionTous;
	@FXML
	Button unselectionTous;
	@FXML
	TextField stringRecherche;

	private ListSelectionBean<T> bean;

	public ListSelection() {
		bean = new ListSelectionBean<>();
		FXMLLoader myFXMLloader = new FXMLLoader();
		myFXMLloader.setLocation(ListSelection.class.getResource("ListSelection.fxml"));
		myFXMLloader.setRoot(this);
		myFXMLloader.setController(this);

		try {
			myFXMLloader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}

		gererDoubleClic();
		initialiserDragAndDrop();
		gererDragOver();
		gererDragDrop();

	}

	/**
	 * initialise drag and drop sur tableDisponible et tableSelectionne
	 */
	private void initialiserDragAndDrop() {

		tableDisponible.setOnDragDetected(dragEvent -> {

			Dragboard dragBoard = tableDisponible.startDragAndDrop(TransferMode.MOVE);
			ClipboardContent content = new ClipboardContent();
			content.putString("Selectionner");
//			if (tableDisponible.getSelectionModel().getSelectedItems().size() == 1) {
//				 content.putString((String) tableDisponible.getSelectionModel().getSelectedItem());	
//			}

			dragBoard.setContent(content);

		});

		tableSelectionne.setOnDragDetected(dragEvent -> {

			Dragboard dragBoard = tableSelectionne.startDragAndDrop(TransferMode.COPY);
			ClipboardContent content = new ClipboardContent();
			content.putString("Selectionner");

//			if (tableSelectionne.getSelectionModel().getSelectedItems().size() == 1) {
//				 content.putString((String) tableSelectionne.getSelectionModel().getSelectedItem());	
//						content.putString((String) tableSelectionne.getSelectionModel().getSelectedItem());
//				}

			dragBoard.setContent(content);

		});

	}

	/**
	 * initialise lach� du drag and drop sur tableDisponible et tableSelectionne
	 */
	private void gererDragDrop() {

		tableSelectionne.setOnDragDropped(dragEvent -> {

			selectSimple();

		});

		tableDisponible.setOnDragDropped(dragEvent -> {

			unselectSimple();

		});

		stringRecherche.setOnDragDropped(event -> {
//			if (tableDisponible.getSelectionModel().getSelectedItem() != null) {
			if (event.getGestureSource() == tableDisponible) {
				stringRecherche.setText(tableDisponible.getSelectionModel().getSelectedItem().toString());
			} else {
				stringRecherche.setText(tableSelectionne.getSelectionModel().getSelectedItem().toString());

			}
			filtrerListe();

		});

	}
	
	/**
	 * initialise drag and drop au dessus de la zone sur tableDisponible  tableSelectionne et field recherche
	 */
	private void gererDragOver() {

		tableSelectionne.setOnDragOver(dragEvent -> {

			if (dragEvent.getGestureSource() == tableDisponible) {
				dragEvent.acceptTransferModes(TransferMode.ANY);

			}

		});

		tableDisponible.setOnDragOver(dragEvent -> {

			if (dragEvent.getGestureSource() == tableSelectionne) {
				dragEvent.acceptTransferModes(TransferMode.ANY);

			}

		});

		stringRecherche.setOnDragOver(event -> {

			if ((event.getGestureSource() == tableSelectionne || event.getGestureSource() == tableDisponible)
					&& ((tableSelectionne.getSelectionModel().getSelectedItems().size() == 1)
							|| (tableDisponible.getSelectionModel().getSelectedItems().size() == 1))) {

				event.acceptTransferModes(TransferMode.ANY);
			}

		});

	}

	/**
	 * gestion double click pour ajour ou supression dans liste
	 */
	private void gererDoubleClic() {
		tableDisponible.setOnMouseClicked(e -> {

			if (e.getClickCount() == 2) {
				selectSimple();
			}

		});

		tableSelectionne.setOnMouseClicked(e ->

		{
			if (e.getClickCount() == 2) {
				unselectSimple();
			}
		}

		);

	}

	@FXML
	private void initialize() {
		tableDisponible.setItems(bean.getListeDisponible());
		tableSelectionne.setItems(bean.getListeSelectionne());

	}

	public void selectSimple() {

		ObservableList<T> t = tableDisponible.getSelectionModel().getSelectedItems();
		System.out.println("Element s�l�ctionn� :" + t);
		if (t != null) {
			bean.addSelectionneSimple(t);
			tableDisponible.getSelectionModel().clearSelection();
		}
	}

	public void unselectSimple() {

		ObservableList<T> t1 = tableSelectionne.getSelectionModel().getSelectedItems();
		if (t1 != null) {
			bean.removeSimple(t1);
			tableSelectionne.getSelectionModel().clearSelection();
		}
	}

	/**
	 * Transfere liste dispo dans liste selectionne
	 */
	public void selectAll() {
		bean.addAll();

	}

	/**
	 * transfere liste Selectionne dans listeDispo
	 */
	public void unselectAll() {
		bean.removeAll();
	}

	/**
	 * ajoute un element t dans la liste des disponible
	 * @param t
	 */
	public void addDisponible(T t) {
		bean.addDisponible(t);
	}

	/**
	 * ajoute element t dans la liste des selectionne
	 * @param t
	 */
	public void addSelectionne(T t) {
		bean.addSelectionne(t);

	}

	/**
	 * listener filtre filtered Listes
	 */
	public void filtrerListe() {

		bean.filtrerListe(stringRecherche.getText());
	}

	/**
	 * Permet d'afficher ou non la zone de recherche
	 * @param hide
	 */
	public void afficherRecherche(Boolean hide) {
		this.getTop().setVisible(hide);

	}

	/**
	 * permet de desactiver la selection multiple
	 * @param choix
	 */
	public void setMultipleSelection(boolean choix) {
		if (choix) {
			tableDisponible.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
			tableSelectionne.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		} else {
			tableDisponible.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
			tableSelectionne.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
		}

	}

}
