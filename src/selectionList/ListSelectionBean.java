package selectionList;

import java.util.ArrayList;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;

/**
 * 
 * @author Aur�lien
 *
 *Bean de gestion de l'affichage des liste Dispo, selectionne et field recherche
 * @param <T>
 */
public class ListSelectionBean<T> {

	private ObservableList<T> listeDisponible;
	private ObservableList<T> listeSelectionne;
	private FilteredList<T> listeDisponibleFiltered;
	private FilteredList<T> listeSelectionFiltered;

	String recherche;

	public ListSelectionBean() {

		listeDisponible = FXCollections.observableArrayList();
		listeSelectionne = FXCollections.observableArrayList();
		listeDisponibleFiltered = new FilteredList<>(listeDisponible, null);
		listeSelectionFiltered = new FilteredList<>(listeSelectionne, null);

		recherche = null;
	}

	public ObservableList<T> getListeSelectionne() {
		return listeSelectionFiltered;
	}

	public ObservableList<T> getListeDisponible() {
		return listeDisponibleFiltered;
	}

	public void addDisponible(T t) {
		System.out.println("Ajout de " + t);
		listeDisponible.add(t);
		System.out.println("listeDispo :" + getListeDisponible());

	}

	public void addSelectionne(T t) {
		System.out.println("Ajout de " + t);

		listeSelectionne.add(t);

		System.out.println("listeSelec :" + getListeSelectionne());
	}

	public void removeSelectionne(ObservableList<T> t) {
		listeSelectionne.removeAll(t);
		listeDisponible.addAll(t);

	}

	public void removeDisponible(ObservableList<T> t) {
		listeDisponible.removeAll(t);

	}

	public void addAll() {

		listeSelectionne.addAll(listeDisponible);
		listeDisponible.clear();
	}

	public void removeAll() {
		listeDisponible.addAll(listeSelectionne);
		listeSelectionne.clear();

	}

	public void filtrerListe(String text) {
		listeDisponibleFiltered.setPredicate(t -> {

			boolean nomSearched = t.toString().contains(text);

			return nomSearched;

		});

		listeSelectionFiltered.setPredicate(t -> {

			boolean nomSearched = t.toString().contains(text);

			return nomSearched;

		});
	}


	public void addSelectionneSimple(ObservableList<T> t) {
		listeSelectionne.addAll(t);
		listeDisponible.removeAll(t);
	}

	public void removeSimple(ObservableList<T> t) {
		listeDisponible.addAll(t);
		listeSelectionne.removeAll(t);		
	}

}
