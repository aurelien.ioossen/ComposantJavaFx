package application;

import controlledNRI.ControlledNri;
import controlledTextField.ControlledTextField;
import javafx.application.Application;
import javafx.stage.Stage;
import selectionList.ListSelection;
import javafx.scene.Scene;

public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			
			//LISTE SELECTION
//			ListSelection<String> ls = new ListSelection<String>();
//			ls.addDisponible("a");
//			ls.addDisponible("b");		
//			ls.addSelectionne("c");
//			ls.afficherRecherche(true);
//			ls.setMultipleSelection(true);

		
			
			// CONTROLLED TEXTFIELD
//			ControlledTextField ctrl =new ControlledTextField("Nom :");
//			ctrl.setPattern("[A-Z]*");
//			primaryStage.setScene(new Scene(ctrl));
//			primaryStage.show();
			
			//CONTROLLED NRI
			ControlledNri ctrlNri = new ControlledNri();
			primaryStage.setScene(new Scene(ctrlNri));
			primaryStage.show();
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}
}
