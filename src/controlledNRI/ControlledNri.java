package controlledNRI;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputControl;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;

import java.io.IOException;

public class ControlledNri extends BorderPane {

	@FXML
	private Label controlledLabel;
	@FXML
	private TextField ctrlKey;

	@FXML
	private TextField ctrlNri;

	private String pattern;
	private String patternPremierChiffre;
	private String patternDeuxTrois;
	private String patternQuatreCinq;
	private String patternSixSept;
	private String patternHuiNeufDix;
	private String patternOnzeDouzeTreize;
	private String nriEntered;
	private Boolean isKeyok;

	public ControlledNri() {

		FXMLLoader myFXMLloader = new FXMLLoader();
		myFXMLloader.setLocation(ControlledNri.class.getResource("ControlledNri.fxml"));
		myFXMLloader.setRoot(this);
		myFXMLloader.setController(this);

		try {
			myFXMLloader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// pattern=".*";

		patternPremierChiffre = "[1-4]||[7-8]";
		patternDeuxTrois = "[0-9]{2}";
		patternQuatreCinq = "0[1-9]||1[0-2]||20||3[0-9]||4[0-2]||[5-9][0-9]";
		patternSixSept = "[0-8][0-9]||9[0-5]||2A||2B||9[6-8]||99";
		patternHuiNeufDix = "[0-9]{3}";
		patternOnzeDouzeTreize = "[0-9]{3}";
		isKeyok = false;

	}

	public String getPattern() {
		return pattern;
	}

	public void setWidth(int longueurSouhaite) {

		controlledLabel.setPrefWidth(longueurSouhaite);

	}

	public void setLabelInvisible(boolean choix) {

		controlledLabel.setVisible(choix);
	}

	@FXML
	public void afficherResultatNri(KeyEvent event) {

		int positionCurseur = ctrlNri.getCaretPosition();
		nriEntered = ctrlNri.getText();

		System.out.println("NriEntered = " + nriEntered);
		System.out.println("Position curseur = " + positionCurseur);

		if (nriEntered.length() > 13 && positionCurseur != 13) {

			nriEntered = nriEntered.substring(0, positionCurseur-1) + nriEntered.substring(positionCurseur);
			
			System.out.println("Nouvelle chaine " +nriEntered);
			ctrlNri.setText(nriEntered);
			ctrlKey.requestFocus();
		}

		switch (positionCurseur) {

		case 1:
			verifierPartie(0, 1, patternPremierChiffre);
			break;
		case 3:
			verifierPartie(1, 3, patternDeuxTrois);
			break;
		case 5:
			verifierPartie(3, 5, patternQuatreCinq);
			break;
		case 7:
			verifierPartie(5, 7, patternSixSept);
			break;
		case 10:
			verifierPartie(7, 10, patternHuiNeufDix);
			break;
		case 13:
			verifierPartie(10, 13, patternOnzeDouzeTreize);
			ctrlKey.requestFocus();
			break;
		case 14:
			String stringTest = nriEntered.substring(0, 13);
			ctrlNri.setText(stringTest);
			ctrlKey.requestFocus();

		}

	}

	private void verifierPartie(int debut, int fin, String pattern) {
		String stringTest = nriEntered.substring(debut, fin);
		if (!stringTest.matches(pattern)) {
			ctrlNri.setStyle("-fx-backround-color: red;");
			System.out.println("Incorrect ! remplacement :" + nriEntered);
			nriEntered = nriEntered.replace(nriEntered.substring(debut, fin), "");
			System.out.println("nriEntered maintenant = " + nriEntered);
			ctrlNri.setText(nriEntered);
			ctrlNri.positionCaret(debut);
		}

	}

	@FXML
	private void afficherResultatKey(KeyEvent event) {
		if (ctrlKey.getLength() == 2) {
			double keyEntered = Double.parseDouble(ctrlKey.getText());

			System.out.println("Key entered = " + keyEntered);

			remplacerCorse();

			Double nriEnteredInteger = Double.parseDouble(nriEntered);
			System.out.println("NRI entered = " + nriEnteredInteger);
			Double keyNri = 97 - (nriEnteredInteger % 97);
			System.out.println("Key VALIDE = " + keyNri);

			controllerKey(keyEntered, keyNri);
		} else {
			ctrlKey.setStyle("-fx-background-color: orange;");
		}
	}

	private void controllerKey(double keyEntered, Double keyNri) {
		if (keyNri == keyEntered) {

			System.out.println("CLE VALIDE");
			ctrlKey.setStyle("-fx-background-color: green;");
			isKeyok = true;

		} else if (keyNri != keyEntered) {
			System.out.println("CLE INVALIDE");
			ctrlKey.setStyle("-fx-background-color: red;");
			isKeyok = false;

		}
	}

	private void remplacerCorse() {

		if (nriEntered.contains("2A")) {
			nriEntered = nriEntered.replace("2A", "19");
			System.out.println("Remplacement 2A par 19");

		} else if (nriEntered.contains("2B")) {
			nriEntered = nriEntered.replace("2B", "18");
			System.out.println("Remplacement 2A par 18");

		}
	}

	public TextField getCtrlKey() {
		return ctrlKey;
	}

	public TextField getCtrlTextField() {
		return ctrlNri;
	}

	public Boolean getIsKeyok() {
		return isKeyok;
	}

}