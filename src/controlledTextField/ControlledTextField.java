package controlledTextField;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;

import java.io.IOException;


public class ControlledTextField extends BorderPane {

    @FXML
    private Label controlledLabel;
    @FXML
    private Label controlledVerification;
    @FXML
    private TextField controlledTextField;

    private String pattern;


    public ControlledTextField() {
        FXMLLoader myFXMLloader = new FXMLLoader();
        myFXMLloader.setLocation(ControlledTextField.class.getResource("ControlledTextField.fxml"));
        myFXMLloader.setRoot(this);
        myFXMLloader.setController(this);



        try {
            myFXMLloader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        controlledTextField.textProperty().addListener(((observableValue, s, t1) -> afficherResultatControle(t1)));

        pattern=".*";

        
    }

    public ControlledTextField(String nomLabel) {

        FXMLLoader myFXMLloader = new FXMLLoader();
        myFXMLloader.setLocation(ControlledTextField.class.getResource("ControlledTextField.fxml"));
        myFXMLloader.setRoot(this);
        myFXMLloader.setController(this);



        try {
            myFXMLloader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        pattern=".*";
        controlledTextField.textProperty().addListener(((observableValue, s, t1) -> afficherResultatControle(t1)));
        controlledLabel.setText(nomLabel);





    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
        //creation tolltip
        Tooltip tooltip = new Tooltip();
        tooltip.setText("Format autoris� = "+pattern);
        controlledTextField.setTooltip(tooltip);
    }


    public void setWidth(int longueurSouhaite){

        controlledLabel.setPrefWidth(longueurSouhaite);

    }

    public void setLabelInvisible(boolean choix){

            controlledLabel.setVisible(choix);
    }


    public void afficherResultatControle(String texteTest) {

        if (texteTest.matches(pattern)) {
            controlledVerification.setText("Correct");
            controlledVerification.setTextFill(Color.LIGHTGREEN);
        }else  {
            controlledVerification.setText("Incorrect");
            controlledVerification.setTextFill(Color.RED);
        }


    }
    
    

}